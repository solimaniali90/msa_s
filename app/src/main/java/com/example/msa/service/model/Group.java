package com.example.msa.service.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Group {

    @SerializedName("items")
    private List<Item> items = null;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
