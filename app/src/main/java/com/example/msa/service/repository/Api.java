package com.example.msa.service.repository;



import com.example.msa.service.model.ExploreBody;
import com.example.msa.service.model.PhotosModel.PhotosBody;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api
{
    // Get venues
    @GET("venues/explore")
    Call<ExploreBody> getVenues(@Query("client_id") String client_id,
                                @Query("client_secret") String client_secret,
                                @Query("ll") String s,
                                @Query("limit") String limit,
                                @Query("venuePhotos") String count,
                                @Query("v") String date);

    // Get venues using the category
    @GET("venues/explore")
    Call<ExploreBody> getVenuesWithCategory(@Query("client_id") String client_id,
                                            @Query("client_secret") String client_secret,
                                            @Query("ll") String s,
                                            @Query("section") String category,
                                            @Query("limit") String limit,
                                            @Query("venuePhotos") String count,
                                            @Query("v") String date);

    @GET("venues/{venue_id}/photos")
    Call<PhotosBody> getPictures(@Path("venue_id") String venue_id,
                                 @Query("client_id") String client_id,
                                 @Query("client_secret") String client_secret,
                                 @Query("limit") String limit,
                                 @Query("v") String date);

}
