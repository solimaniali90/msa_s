package com.example.msa.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



import com.example.msa.R;


import com.example.msa.service.model.Venue;

import com.example.msa.view.ui.Activity.VenuePhotoActivity;
import com.squareup.picasso.Picasso;

import java.util.List;



public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity context;
   List<Venue> userArrayList;

    public RecyclerViewAdapter(Activity context, List<Venue> userArrayList) {
        this.context = context;
        this.userArrayList = userArrayList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.item,parent,false);

        return new RecyclerViewViewHolder(rootView);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        Venue venue = userArrayList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;

        viewHolder.txtView_title.setText(venue.getName());
        viewHolder.txtView_description.setText(venue.getLocation().getAddress());

    String url = venue.getCategories().get(0).getIcon().getURLforThumbnail();
        Picasso.with(this.context)
                .load(url)
                .placeholder(R.drawable.placeholder_image) // display this picture as a placeholder
                .error(R.drawable.error_img)           // display this picture if error occurs
                .into(viewHolder.imgView_icon);

        Picasso.with(this.context)
                .load(url)
                .placeholder(R.drawable.placeholder_image) // display this picture as a placeholder
                .error(R.drawable.error_img)           // display this picture if error occurs
                .into(viewHolder.thumbnail_imageview);



        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(context, VenuePhotoActivity.class);

                String name_venue = venue.getName();
                String id_venue = venue.getId();
                String address_venue= venue.getLocation().getAddress();

                intent.putExtra("name_venue", name_venue);
                intent.putExtra("id_venue",id_venue);
                intent.putExtra("address_venue", address_venue);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {

        return userArrayList.size();
    }

    static class RecyclerViewViewHolder extends RecyclerView.ViewHolder {
        ImageView imgView_icon,thumbnail_imageview;
        TextView txtView_title;
        TextView txtView_description;

        public RecyclerViewViewHolder(@NonNull View itemView) {
            super(itemView);
            imgView_icon = itemView.findViewById(R.id.imgView_icon);
            txtView_title = itemView.findViewById(R.id.txtView_title);
            txtView_description = itemView.findViewById(R.id.txtView_description);

            thumbnail_imageview=itemView.findViewById(R.id.thumbnail_imageview);
        }
    }
}
