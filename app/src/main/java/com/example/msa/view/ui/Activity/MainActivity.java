package com.example.msa.view.ui.Activity;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.msa.R;


import com.example.msa.service.model.Venue;
import com.example.msa.view.adapter.RecyclerViewAdapter;
import com.example.msa.viewmodel.GpsTracker;
import com.example.msa.viewmodel.MainViewModel;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import static android.content.pm.PackageManager.PERMISSION_DENIED;

public class MainActivity extends AppCompatActivity {
    MainActivity context;
    MainViewModel viewModel;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    private GpsTracker gpsTracker;


    ProgressBar progressBar;
    public static String latitude_id ;
    public static String longitude_id ;
    private ActivityResultLauncher<String> requestPermissionLauncher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView = findViewById(R.id.rv_main);
        requestPermissionLauncher = registerForActivityResult(
                new ActivityResultContracts.RequestPermission(), isGranted -> {
                    if (isGranted) {
                        Load();
                    } else {
                        Snackbar.make(
                                findViewById(R.id.activity_tab),
                                R.string.permission_rationale,
                                Snackbar.LENGTH_LONG)
                                .show();

                    }
                });
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PERMISSION_DENIED) {

            requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION);

        }else {
            Load();
        }

    }



    public void Load(){
        if (isNetworkAvailable()) {
            Toast.makeText(context,"Network",Toast.LENGTH_SHORT).show();
            gps();

        }else {

            progressBar.setVisibility(View.GONE);
            Toast.makeText(context,"no Network",Toast.LENGTH_SHORT).show();

        }
    }



    public void gps(){
        gpsTracker = new GpsTracker(MainActivity.this);
        if(gpsTracker.canGetLocation()){
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            latitude_id= String.valueOf(latitude);
            longitude_id= String.valueOf(longitude);
            intin();

        }
        else{
            gpsTracker.showSettingsAlert();
        }
    }
    public void intin(){

        recyclerView.setHasFixedSize(true);
        viewModel = ViewModelProviders.of(context).get(MainViewModel.class);
        viewModel.getAlbum().observe(context, userListUpdateObserver);
    }

    Observer<List<Venue>> userListUpdateObserver = new Observer<List<Venue>>() {
        @Override
        public void onChanged(List<Venue> userArrayList) {
            recyclerViewAdapter = new RecyclerViewAdapter(context,userArrayList);
            recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
            recyclerView.setAdapter(recyclerViewAdapter);
            progressBar.setVisibility(View.GONE);

        }
    };

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void loaddata(View view) {
        Load();
    }
}