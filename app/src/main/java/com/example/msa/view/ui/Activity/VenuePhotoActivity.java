package com.example.msa.view.ui.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.msa.R;

import com.example.msa.service.model.PhotosModel.PhotoItem;

import com.example.msa.view.adapter.RecyclerViewPhotoAdapter;
import com.example.msa.viewmodel.MainViewModel;

import java.util.List;

public class VenuePhotoActivity extends AppCompatActivity {
    public static String VENUE_ID = null;
    VenuePhotoActivity context;

    MainViewModel viewModel;
    RecyclerView recyclerView;
    RecyclerViewPhotoAdapter recyclerViewPhotoAdapter;

    TextView name_view,address_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_photo);
        context = this;
        Bundle myBundle = getIntent().getExtras();

        String venueName = myBundle.getString("name_venue");
        String venusid = myBundle.getString("id_venue");
        String address = myBundle.getString("address_venue");
        VENUE_ID = venusid;

        name_view = findViewById(R.id.name_xml);
        address_view = findViewById(R.id.address_xml);

        recyclerView = findViewById(R.id.recycler_xml);

        name_view.setText(venueName);
        address_view.setText(address);

        Toast.makeText(VenuePhotoActivity.this, venueName, Toast.LENGTH_SHORT).show();
       // recyclerView.setHasFixedSize(true);

        viewModel = ViewModelProviders.of(context).get(MainViewModel.class);
        viewModel.getPhoto().observe(context, PhotoListUpdateObserver);
    }
    Observer<List<PhotoItem>> PhotoListUpdateObserver = new Observer<List< PhotoItem>>() {
        @Override
        public void onChanged(List<PhotoItem> userArrayList) {

            recyclerViewPhotoAdapter= new RecyclerViewPhotoAdapter(context,userArrayList);
            recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
            recyclerView.setAdapter(recyclerViewPhotoAdapter);

        }
    };

}