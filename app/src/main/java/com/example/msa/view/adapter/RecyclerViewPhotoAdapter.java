package com.example.msa.view.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.msa.R;
import com.example.msa.service.model.PhotosModel.PhotoItem;
import com.example.msa.service.model.Venue;
import com.example.msa.view.ui.Activity.MainActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewPhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    Activity context;
    List<PhotoItem> photoArrayList;

    public RecyclerViewPhotoAdapter(Activity context, List<PhotoItem> photoArrayList) {
        this.context = context;
        this.photoArrayList = photoArrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.photo_item,parent,false);

        return new RecyclerViewViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PhotoItem photoItem=photoArrayList.get(position);
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;
        String url = photoItem.getURLforGrid();
        Picasso.with(this.context)
                .load(url)
                .placeholder(R.drawable.placeholder_image) // show this if no image received
                .error(R.drawable.error_img)           // show this when error occurs
                .into( viewHolder.photo_item);

    }

    @Override
    public int getItemCount() {
        return photoArrayList.size();
    }

    static class RecyclerViewViewHolder extends RecyclerView.ViewHolder {
        ImageView photo_item;

        public RecyclerViewViewHolder(@NonNull View itemView) {
            super(itemView);
            photo_item=itemView.findViewById(R.id.photo_xml);
        }
    }
}
